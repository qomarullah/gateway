module gitlab.com/kopsyahmtt/gateway

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/labstack/echo/v4 v4.2.1
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/spf13/cast v1.3.0
	github.com/spf13/viper v1.7.1
	github.com/tylerb/graceful v1.2.15
	go.uber.org/zap v1.16.0
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324
)
