package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/spf13/cast"
	"github.com/spf13/viper"
	"github.com/tylerb/graceful"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"golang.org/x/time/rate"
)

type App struct {
	AppName   string   `json:"appName"`
	Version   string   `json:"version"`
	Port      string   `json:"port"`
	Timeout   int      `json:"timeout"`
	Logger    string   `json:"logger"`
	JwtSecret string   `json:"jwtSecret"`
	JwtExpiry int      `json:"jwtExpiry"`
	Member    []Member `json:"member"`
}

type Member struct {
	Prefix  string   `json:"prefix"`
	Auth    bool     `json:"auth"`
	Limiter int      `json:"limiter"`
	Target  []string `json:"target"`
}

type jwtCustomClaims struct {
	Data map[string]interface{} `json:"data"`
	jwt.StandardClaims
}
type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Data    Data   `json:"data"`
}
type Data struct {
	Token string `json:"token"`
}

var logger *zap.Logger

func main() {

	//config
	var config App
	env := os.Getenv("env")
	if env == "" {
		env = "default"
	}
	viper.SetConfigName("config." + env)
	viper.SetConfigType("json")
	viper.AddConfigPath("config")
	viper.AddConfigPath(".")    // optionally look for config in the working directory
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	err = viper.Unmarshal(&config)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	//logger := log.New(os.Stdout, "logger: ", log.Lshortfile)
	logFile := config.Logger + "-%Y%m%d.log"
	rotator, err := rotatelogs.New(
		logFile,
		rotatelogs.WithMaxAge(60*24*time.Hour),
		rotatelogs.WithRotationTime(time.Hour))
	if err != nil {
		panic(err)
	}

	// initialize the JSON encoding config
	encoderConfig := map[string]string{
		//"levelEncoder": "capital",
		"timeKey":     "time",
		"timeEncoder": "RFC3339",
	}
	data, _ := json.Marshal(encoderConfig)
	var encCfg zapcore.EncoderConfig
	if err := json.Unmarshal(data, &encCfg); err != nil {
		panic(err)
	}

	// add the encoder config and rotator to create a new zap logger
	w := zapcore.AddSync(rotator)
	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(encCfg),
		w,
		zap.InfoLevel)
	logger = zap.New(core)

	e := echo.New()
	for _, v := range config.Member {

		targets := []*middleware.ProxyTarget{}
		for _, k := range v.Target {
			urlTarget, err := url.Parse(k)
			if err != nil {
				e.Logger.Fatal(err)
			}
			proxy := middleware.ProxyTarget{URL: urlTarget}
			targets = append(targets, &proxy)
		}

		g := e.Group(v.Prefix)
		if v.Auth == true {
			g.Use(middleware.JWTWithConfig(middleware.JWTConfig{
				SigningKey: []byte(config.JwtSecret),
				//TokenLookup: "query:token",
			}), ServerHeader)
			//g.Use(ServerHeader)
		}

		/*g.Use(middleware.BodyDump(func(c echo.Context, reqBody, resBody []byte) {
			logger.Sugar().Infow("gateway", zap.Any("path", c.Request().URL.Path), zap.Any("req", reqBody), zap.Any("resp", resBody))

		}))
		*/
		g.Use(middleware.Proxy(middleware.NewRoundRobinBalancer(targets)))

		if v.Limiter > 0 {
			g.Use(middleware.RateLimiter(middleware.NewRateLimiterMemoryStore(rate.Limit(v.Limiter))))
		}

	}
	//get token
	e.POST("/v1/auth", func(c echo.Context) error {

		var bodyBytes []byte
		if c.Request().Body != nil {
			bodyBytes, _ = ioutil.ReadAll(c.Request().Body)
		}
		//json to struct
		var bodyJwt map[string]interface{}
		if err := json.Unmarshal(bodyBytes, &bodyJwt); err != nil {
			log.Fatal(err)
		}

		claims := jwtCustomClaims{}
		claims.Data = bodyJwt
		claims.ExpiresAt = time.Now().Add(time.Hour * time.Duration(config.JwtExpiry)).Unix()
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		// Generate encoded token and send it as response.
		t, err := token.SignedString([]byte(config.JwtSecret))
		if err != nil {
			return err
		}
		resp := &Response{
			Status:  "00",
			Message: "Success",
			Data:    Data{Token: t},
		}
		return c.JSON(http.StatusOK, resp)

	})

	e.Use(middleware.BodyDump(func(c echo.Context, reqBody, resBody []byte) {
		logger.Sugar().Infow("header-request", zap.Any("header", c.Request().Header))
		logger.Sugar().Infow("gateway", zap.String("path", c.Request().URL.Path), zap.String("req", string(reqBody)), zap.String("resp", string(resBody)))
		//logger.Sugar().Infow("header-response", zap.Any("header", c.Response().Header))

	}))

	e.Server.Addr = ":" + config.Port
	e.Server.ReadTimeout = time.Duration(config.Timeout) * time.Minute
	e.Server.WriteTimeout = time.Duration(config.Timeout) * time.Minute
	log.Print("server run .." + config.Port)
	graceful.ListenAndServe(e.Server, 5*time.Second)

}

// ServerHeader middleware adds a `Server` header to the response.
func ServerHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		logger.Sugar().Infow("header-before-jwt", zap.Any("header-before-jwt", c.Request().Header))

		c.Response().Header().Set(echo.HeaderServer, "EchoGW/1.0")
		claims := jwtCustomClaims{}
		user := c.Get("user").(*jwt.Token)
		tmp, _ := json.Marshal(user.Claims)
		_ = json.Unmarshal(tmp, &claims)

		for key, value := range claims.Data {
			logger.Sugar().Infow("header-jwt:", zap.String(key, cast.ToString(value)))
			c.Request().Header.Set(key, cast.ToString(value))
		}
		return next(c)
	}
}
